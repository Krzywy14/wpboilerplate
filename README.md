# Wordpress RItheme 1.2.0

All template changes make in "_project" directory.
## To use this template required, global instaled
- [node.js](https://nodejs.org/en/)
- [gulp.js](http://gulpjs.com)
- [SCSS](http://sass-lang.com)
- [Yarn](https://yarnpkg.com/en/)
- [ImageMagick](https://www.imagemagick.org/script/download.php) and [GraphicsMagick](http://www.graphicsmagick.org/download.html)

## REMEBER
Before do anything replace all "RItheme" word to you project name :)

## Gulp
### What is gulp?
Gulp is a task runer, which makes easy, speeds up and automates your work.
[Gulp.js](https://gulpjs.com)

### Gulp tasks - Used in this project
- Clean - Clear dest directory.
- [Images](https://github.com/1000ch/gulp-image) - Compress images and save in dest direcory. [Require IM](https://www.imagemagick.org/script/download.php) and [Require GM](http://www.graphicsmagick.org/download.html)
- Scripts - Import, convert, compress scripts and save in dest directory.
- Styles - Import and compress sass styles and save in dest directory.
- Move - Move other files to dest folder
- [Watch](https://github.com/floatdrop/gulp-watch) - Check files changes and reload browser by [BrowseSync](https://browsersync.io/docs).
- [Sql](https://github.com/webcaetano/mysqldump) - Data base backup.

### Suggest to use technologies
- [BEM](https://nafrontendzie.pl/metodyki-css-2-bem/)
- [SASS/SCSS](http://www.merixstudio.pl/blog/co-daje-sass-dlaczego-warto-uzywac-sass/)
- [es2015+](https://babeljs.io/learn-es2015/) [PL](http://mateuszroth.pl/komp/article-javascriptes6.php)


## Geting start

1. Download and install wordpress.
2. By comand line get into wp-content/themes.
```bash
cd wp-content/themes/
```
3. Clone repository to create dir. Remember clone into "_project" dir.
```bash
git clone git@bitbucket.org:Krzywy14/stock-template.git <theme name>
```
4. Change git repository to your own.
```bash
git config remote.origin.url git@bitbucket.org:Krzywy14/nowe.git
```
5. Get into _project folder.
```bash
cd _project
```
6. Install required modules by package manager. Recomend [yarn](https://yarnpkg.com/en/)
```bash
yarn install
```
7. Change gulp configuration if you need, in "config.json" file. Only in this line.
```
"BS":{
	"proxy": 		"wordpress.dev",<- to [your vhost]
	"logLevel":		"info"			<- to [debug or silent]
},
"images": {
	"width": 		"1920",			<- to [image max width]
	"crop": 		"false",		<- to [true, if you want to crop img not resize, not recomend]
	"upscale": 		"false",		<- to [true, if you want to upscale samle image, not recomend]
},
"sql":{
	"host": 		"localhost", 	<- to [your database host]
	"user": 		"root",			<- to [your database user]
	"password": 	"",				<- to [your database pass]
	"database": 	"syncret_db" 	<- to [your database name]
}

```
[log level](https://browsersync.io/docs/options#option-logLevel) [output styles](https://web-design-weekly.com/2014/06/15/different-sass-output-styles/)

## Use Gulp
1. Witch watch, type comand:
```bash
gulp run
```
2. Once run
```bash
gulp
```
3. Database backup
```bash
gulp sql
```
4. Clear dest dir
```bash
gulp clean
```
5. Images - gulp not always find new image file, so use:
```bash
gulp images
```
5. Build final poroduct(compress css and js)
```bash
gulp build
```

* * *

## Include
- [Normalize.scss](http://nicolasgallagher.com/about-normalize-css/)
- [Bootstrap v4 - beta](https://getbootstrap.com/docs/4.0/)
- [Responsive navigation](https://codepen.io/Krzywy14/pen/WpMwXN)
- Plugins
	- [Tabs swither](#tab-switcher)
	- [Scroll to top](#scroll-to-top)
	- [Scroll stop](#scroll-stop)
	- [Anchor navigation](#anchor-navigation)
	- [Parallax](#parallax)
	- [Cookies](#cookies)
	- [Counters](#counters)
	- [Accordion](#accordion)
	- [Gallery](#gallery)
	- [Curtain](#curtain)
	- [Word sizer](#word-sizer)
	- [Preloader](#preloader)
	- [Image zoom on hover](#image-zoom-on-hover)
- IE 10 if
- [Random string length genrator(Lorem Ipsum)](#Random-text-generator)
- Wordpress fucntion:
	- Custom content lenght
	- Image functions:
		- [get_img](#simple) - Return html <img> with src and alt. Based on assets/img dir.
		- [get_bg](#simple-background) - Return html inline styl with background-image. Based on assets/img dir.
		- [get_imgth](#WP-thumbnail) - Return html <img> with src and alt. Based on WP tumbnail img.
 		- [get_bgth](#WP-thumbnail-background) - Return html inline styl with background-image. Based on WP thumbnail.
	- Custom login page - must be active in function.php
	- Remove dashboard menu item - must be active in function.php
	- Remove editor from dashboard
* * *

#Changelog
## 1.2.0
- Update Gulp to 4.0
- Rebuild task for gulp 4.0
- Fix Build task
## 1.1.3
- package.json update
- Lightbox Update( add gesture/swipe, add arrow key trigers, add counter, add escape close)
- Add rect(height) mixin, lifehac for easy squeres or rects always responsiwe
- Add RWD lazy load background images php ang js, get_rwdbg($postID=null,$smallSize='medium_large',$hugeSize='large',$classes='')
## 1.1.2
- Rebuild cookies plugin to localstorage
- Update gulp task, Add produciton script and style task
- Remove nodeEnv, css compres from config
- Include default cookies plugin 
- Rebuild WP helpers 'echo' to 'return'
- fixes typo
- 404 translation add
- add default translation template
- fix header logo
- fix copyright
## 1.1.1
- Add Wordpress 5 block styles
- Update and rebuild gallery/lightbox
- Add lightbox swipe
- Move wp core style to components/wp/*
- Trun off default wp-block styles at frontend