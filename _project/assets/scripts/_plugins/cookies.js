export default () => {
	const setCookie = () => {
		localStorage.setItem('cookiesAccepted', 'true');
	}

	const getCookie = () => {
		return localStorage.getItem('cookiesAccepted');
	}

	const checkCookie = () => {
		if ( getCookie() == 'true' ){
			document.querySelector("#cookieBar").classList.remove('active');
		} else {
			document.querySelector("#cookieBar").classList.add('active');
		}
	}
	checkCookie();
	document.querySelector("#cookieBarAccept").addEventListener('click',()=>{
		setCookie();
		checkCookie();
	});
};
