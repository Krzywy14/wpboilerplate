'use strict'

import gulp			from 'gulp';
import config		from './tasks/config';
import browserSync	from 'browser-sync';
import runSequence 	from 'run-sequence';

// gulp tasks

import styles, {StylesProd}		from "./tasks/styles";
import clean					from "./tasks/clean";
import move				 		from "./tasks/move";
import scripts, {ScriptsProd}	from "./tasks/scripts";
import customizer 				from "./tasks/customizer";
import images					from "./tasks/images";
import watch					from "./tasks/watch";
import sql						from "./tasks/sql";


gulp.task('styles',			styles);
gulp.task('styles:prod',	StylesProd);
gulp.task('move',			move);
gulp.task('scripts',		scripts);
gulp.task('scripts:prod',   ScriptsProd);
gulp.task('customizer',		customizer);
gulp.task('images',			images);
gulp.task('clean',			clean);
gulp.task('watch',			watch);
gulp.task('sql',			sql);

gulp.task("default", gulp.series(['styles','scripts','customizer','move','images'], (done)=>{
    done();
}))

gulp.task("build", gulp.series(['styles:prod','scripts:prod','customizer','move','images'], (done)=>{
    done();
}))

gulp.task("run", gulp.series(['default','watch'], (done)=>{
    done();
}))