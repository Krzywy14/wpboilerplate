'use strict';

import gulp			from 'gulp';
import config		from './config';
import reportErr	from './notify';
import browserSync	from 'browser-sync';

import babel		from 'gulp-babel';
import babelify		from 'babelify';
import browserify	from 'browserify';
import source		from 'vinyl-source-stream';
import buffer		from 'vinyl-buffer';
import uglify 		from 'gulp-uglify';


export default () => {
	process.env.NODE_ENV = config.customizer.nodeENV;
	let bundler = browserify( config.customizer.source );
	bundler.transform( "babelify", {
		presets: ['@babel/preset-env'],
	});

	return bundler.bundle()
		.on('error', function(error) {
			reportErr(error, this);
		})
		.pipe( source('customizer.js') )
		.pipe( buffer() )
		.pipe( uglify() )
		.pipe( gulp.dest( config.customizer.destination ) )
		.pipe( browserSync.stream() );
}