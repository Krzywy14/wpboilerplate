'use strict';

import gulp		from 'gulp';
import notify	from 'gulp-notify';

export default (error, self) => {
	/*===================================
	=            Console log            =
	===================================*/
	
	console.log('== Error - '+error.plugin+' ==============================================\nPlugin: '+error.plugin+'\n'+error.message+'\n== End of Error - '+error.plugin+'==============================================\n');
	
	/*=====  End of Console log  ======*/
	
	

	/*=====================================
	=            Notifications            =
	=====================================*/
	
	if (error.line && error.column) {
		// var notifyMessage = 'LINE ' + error.line + ':' + error.column;
		var notifyMessage = 'Line: ' + error.line + ' in ' + error.file;
	} else {
		var notifyMessage = '';
	}

	notify({
		title: 'FAIL ' + error.plugin,
		message: notifyMessage+'\nCheck the console',
		sound: 'Frog'
	}).write(error);
	
	/*=====  End of Notifications  ======*/
	
	

	self.emit('end');
}
