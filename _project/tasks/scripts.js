'use strict';

import gulp			from 'gulp';
import config		from './config';
import reportErr	from './notify';
import browserSync	from 'browser-sync';

import babel		from 'gulp-babel';
import babelify		from 'babelify';
import browserify	from 'browserify';
import source		from 'vinyl-source-stream';
import buffer		from 'vinyl-buffer';
import uglify 		from 'gulp-uglify';
import saveLicense	from 'uglify-save-license';


export default () => {
	process.env.NODE_ENV = 'development';

	const extensions = ['.js', '.es6', '.jsx'];
	process.env.NODE_ENV = 'development';
	let bundler = browserify(config.scripts.source,{
		extensions: extensions,
		debug: true
	});
	
	bundler.transform( "babelify", {
		presets: ['@babel/preset-env'],
	});
	
	return bundler.bundle()
		.on('error', function(error) {
			reportErr(error, this);
		})
		.pipe( source('scripts.js') )
		.pipe( buffer() )
		.pipe( gulp.dest( config.scripts.destination ) )
		.pipe( browserSync.stream() );
}

export let ScriptsProd = () => {
	const uglifyOptions = {
		output: {
			comments: saveLicense
		}
	}
	process.env.NODE_ENV = 'production';

	let bundler = browserify( config.scripts.source );
	
	bundler.transform( "babelify", {
		presets: ['@babel/preset-env'],
	});
	
	return bundler.bundle()
		.on('error', function(error) {
			reportErr(error, this);
		})
		.pipe( source('scripts.js') )
		.pipe( buffer() )
		.pipe( uglify(uglifyOptions) )
		.pipe( gulp.dest( config.scripts.destination ) )
		.pipe( browserSync.stream() );
}

