'use strict';

import gulp 		from 'gulp';
import watch		from 'gulp-watch';
import config		from './config'

import browserSync	from 'browser-sync';

export default () => {
	browserSync.create();
	browserSync.init({
		ui: {
		    port: '3001'
		},
		port: 		config.BS.port,
        proxy: 		config.BS.proxy,
        logLevel: 	config.BS.logLevel,
        files: [
			{
			match: ["../**/*.+(php|html)"],
				fn: function (event, file) {
					browserSync.reload();
				},
			}
		]

    });
	gulp.watch( config.styles.source, 		gulp.parallel(['styles']) );
	gulp.watch( config.images.source, 		gulp.parallel(['images']) );
	gulp.watch( config.scripts.allsource, 	gulp.parallel(['scripts']) );
}