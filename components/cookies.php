<div class="cookie-bar__wrap" id="cookieBar">
	<div class="cookie-bar__desc">
		<?= __( 'This site uses cookies to work properly.', 'RItheme' ) ?>
		<a href="<?php get_template_directory_uri();?>/polityka-prywatnosci" class="cookie-bar__more"><?= __( 'More', 'RItheme' ) ?></a>
	</div>
	<div class="cookie-bar__accept" id="cookieBarAccept"><?= __( 'Accept', 'RItheme' ) ?></div>
</div>