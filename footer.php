<?php
/**
 *
 * @package RItheme
 */

?>
	<footer class="parent parent--footer">
		<section class="row container footer__wrap">
			Footer
		</section>
		<section class="row container copyright__wrap">
			&copy; <?php bloginfo('title'); ?> <?= date("Y"); ?>&nbsp;| <span>&nbsp;<?= __( 'Project and realisation', 'RItheme' ) ?>:</span>&nbsp;<a href="http://raczejinaczej.pl">raczejinaczej.pl</a>
		</section>
	</footer>
</div> 
<link rel="stylesheet" id="scss-css" href="<?php echo get_template_directory_uri(); ?>/assets/styles/style.css" type="text/css" media="all">
<?php wp_footer(); ?>
</body>
</html>