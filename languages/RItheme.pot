#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: RItheme\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-12-18 12:05+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/\n"
"X-Loco-Version: 2.2.0; wp-5.0.1"

#: components/cookies.php:6
msgid "Accept"
msgstr ""

#: functions.php:74
msgid "Add widgets here."
msgstr ""

#: 404.php:16
msgid "Back to main page"
msgstr ""

#. URI of the theme
#. Author URI of the theme
msgid "http://raczejinaczej.pl"
msgstr ""

#. Author of the theme
msgid "Krzywy14@gmail.com for http://raczejinaczej.pl"
msgstr ""

#: components/cookies.php:4
msgid "More"
msgstr ""

#: functions.php:45
msgid "Primary"
msgstr ""

#: footer.php:13
msgid "project and realisation"
msgstr ""

#. Description of the theme
msgid "Raczejinaczej Wordpress Theme"
msgstr ""

#. Name of the theme
msgid "RItheme"
msgstr ""

#: functions.php:72
msgid "Sidebar"
msgstr ""

#: components/cookies.php:3
msgid "This site uses cookies to work properly."
msgstr ""

#: 404.php:14
msgid ""
"Unfortunately, the page you are trying to access is not available or does "
"not exist"
msgstr ""
